package it.bper;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.sql.Connection;
import java.util.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import java.util.UUID;
import java.util.regex.Pattern;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.github.zafarkhaja.semver.ParseException;

import generated.ERRORLOG;
//import bper.raccanag.RaccordoAnagrafe;
import generated.TracciatoOutput;
import generated.ZIPLOG;

public class Utils {

	private String propertiesPath;
	private static Logger log = null;
	private static final String osSeparatore = File.separator;
	private static final String INTESTAZIONE_ZIP = "BPR.DOC.DOC";
	private static final String INTESTAZIONECSVPRINCIPALE = "BPR.DOC.DOC";
	private static final String INTESTAZIONECSVPRINCIPALE2 = "BPR.DOC.DOCS";
	private static final String INTESTAZIONECSVSUPPORTO = "BPR.DOC.PRA";
	int numberParamenters = 0;
	String rootPath;
	String NomeInput;
	String TracciatoType;
	String TracciatoOutputType;
	private String xmlOutputPath;
	private String xmlOutputName;
	private String directoryInElab;
	Connection myConn;
	PreparedStatement stmt;
	private String estensione = "";
	String pdfname = "";
	String dataDoc;
	String URL="";
	String USR="";
	String PWD="";
	String nomeDirectory="";
	String nomeTracciatoOutput;
	int numeroRigheElab=0;
	int numeroRigheCsv=0;
	ERRORLOG errorLog;
	Utils(String outputDir, String propertiesPath) {
		xmlOutputPath = outputDir;
		this.propertiesPath = propertiesPath;
		this.log= Logger.getLogger("Utils");
	}

	private void creaObjectFromCSVLine(String[] intestazioneCampi, String[] valoriRiga,
			String[] intestazioneCampiSupporto, String[] valoriRigaSupporto, TracciatoOutput obj,
			Map<String, String> csvFieldsToOutputfields) throws Exception {
		pdfname = "";
		dataDoc="";

		if (intestazioneCampi.length > 0) {
			boolean estensioneTrovata = false;
			for (int i = 0; i < intestazioneCampi.length; i++) {
				if (intestazioneCampi[i].equals("ESTENSIONE") || intestazioneCampi[i].equals("A_CONTENT_TYPE")) {
					estensione = valoriRiga[i];
					estensioneTrovata = true;
				}

			}

			if (!estensioneTrovata && intestazioneCampiSupporto != null && intestazioneCampiSupporto.length > 0) {
				for (int i = 0; i < intestazioneCampiSupporto.length; i++) {
					if (intestazioneCampiSupporto[i].equals("ESTENSIONE")
							|| intestazioneCampiSupporto[i].equals("ESTENSIONE")) {
						estensione = valoriRigaSupporto[i];
					}

				}
			}

		}

		Field[] allFields = obj.getClass().getDeclaredFields();
		for (Entry<String, String> entry : csvFieldsToOutputfields.entrySet()) {
			String entryValue = entry.getValue().trim();
			String valore = null;
			Field field = null;
			String campo = entry.getKey().toString().toLowerCase();
			String tmpField = entryValue.split("#")[0];
			if (entryValue.equals(""))
				continue;
			switch (entryValue.split("#")[1].trim()) {
			case "CSV":
				String formato = entryValue.split("#")[3].trim();
				String dove = entryValue.split("#")[2].trim();
				String[] intest = null;
				String[] valor = null;
				if (dove.equals("PRINCIPALE")) {
					intest = intestazioneCampi;
					valor = valoriRiga;
				} else {
					intest = intestazioneCampiSupporto;
					valor = valoriRigaSupporto;
				}
				for (int i = 0; i < intest.length; i++) {
					if ((intest[i].equals(tmpField))||(intest[i].equals("R_CREATION_DATE")&&tmpField.equals("DATA"))||
						(nomeTracciatoOutput.equals("REP01CASSSICDGEPRAREMSEL")&&intest[i].equals("DATA_DOCUMENTO")&&tmpField.equals("DATA"))) {
						valore = valor[i].replace("'"," ");
						break;
					}
				}
				
				if (formato.equals("DATA")) {
					
					String useoralegale=entryValue.split("#")[4].trim();
					if (campo.equals("workdate")) {
						String dataValue=valore.split(" ")[0];
						if (dataValue!=null && !dataValue.equals(""))
							dataDoc = valore.split(" ")[0];
					}
					
//					if (campo.equals("datatipoazione") && dataDoc.equals("") ) {
//						dataDoc = valore.split(" ")[0];
//					}

					try {
						if (!"".equals(valore) && valore!=null)
							valore=formatDate(valore,useoralegale);
						if (campo.equals("databonifica")&&("".equals(valore) || valore==null)) {
							valore=formatDate("01/01/1970",useoralegale);
						}
						
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					
//					String[] datavalues = valore.split("/");
//
//					valore = datavalues[2] + "-" + datavalues[1] + "-" + datavalues[0] + "T00:00:00.000+01:00";

				}

				if (formato.equals("DECIMAL")) {
					valore = valore.replace(",", ".");
				}
				
				if (formato.equals("NUMERIC")){
					if (!isNumericRegEx(valore)) {
						throw new Exception(campo + " = " + valore+ " valore non numerico o non intero");
					}
				}
				
				break;
				
			case "PROPERTIES":
				String valoreField = null;
				for (int i = 0; i < intestazioneCampi.length; i++) {
					if (intestazioneCampi[i].equals(tmpField)) {
						valoreField = valoriRiga[i];
						break;
					}
				}
				
////				//LOGICA TAGLIO CODOCUMENTO
//////				if (campo.equals("iafc")||campo.equals("codappl")) {
////				if (campo.equals("codappl")) {
////					String[] splitted = valoreField.split("_");
////					String lastSubstring = splitted[splitted.length - 1];
////					if (lastSubstring.length() == 3 && isNumeric(lastSubstring)) {
////						String toDelete = "_" + lastSubstring;
////
////						valoreField = valoreField.replace(toDelete, "");
////
////					}
//
//				}
				
				if (valoreField != null) {
					String fields[] = { valoreField };
					String[] properties = entryValue.split("#")[2].split("-");
					int propertiesIndex = 0;
					String propertiesName = "";
					if (properties.length == 2) {
						propertiesName=properties[0]+osSeparatore+properties[1];
					} else if (properties.length == 1){
						propertiesName=properties[0];
					}
					else {
						for (int i = 0; i < properties.length; i++) {
							if (propertiesIndex == properties.length - 2) {
								propertiesName += properties[i];
							} else {
								propertiesName += properties[i] + osSeparatore;
							}

						}
					}
					valore = extractProperties(propertiesPath + "" + propertiesName, fields, false).get(fields[0]);
				} 

				break;

			case "OPERATION":
				String operazione = entryValue.split("#")[0];
				switch (operazione) {
				case "ADD":
					String valuePlace = entryValue.split("#")[2];
					switch (valuePlace) {
					case "CSV":
						String[] addFields = entryValue.split("#")[3].split("\\*");
						String[] addValues = new String[addFields.length];
						for (int j = 0; j < addValues.length; j++) {
							String where = addFields[j].split("--")[1];
							String campoCsv = addFields[j].split("--")[0];
							String[] intestazione = null;
							String[] valori = null;
							if (where.equals("PRINCIPALE")) {
								intestazione = intestazioneCampi;
								valori = valoriRiga;
							} else {
								intestazione = intestazioneCampiSupporto;
								valori = valoriRigaSupporto;
							}
							for (int i = 0; i < intestazione.length; i++) {
								if (intestazione[i].equals(campoCsv)) {
									addValues[j] = valori[i];
									break;
								}
							}
						}

						valore = concatenate(addValues, ".");
						break;

					}// end switch valuePlace
					break;
				}// end switch operazione

			}// end switch entryValue.split("#")[1]
			try {
				Class<?> c = obj.getClass().getSuperclass();
				try {
					field = obj.getClass().getDeclaredField(campo);
				} catch (Exception e) {
					//field = c.getClass().getDeclaredField(campo);
					throw new Exception(e.getMessage()+" proprieta java non trovata");
				}
				field.setAccessible(true);

//					if(campo.equals("attachments")){
//						
//						List<TracciatoOutput.Attachments.Attachment> attList= new ArrayList<TracciatoOutput.Attachments.Attachment>();		
//						attList.add(new TracciatoOutput.Attachments.Attachment(valore,valore));
//						Attachments atts = new Attachments(attList);
//						pdfname=valore;
//						field.set(obj, atts);
//					} else {
				
//				if (valore!=null)
//					valore=valore.replaceAll("[^a-zA-Z0-9]", " ");
				
				field.set(obj, valore);
//					}
			} catch (NoSuchFieldException | IllegalArgumentException | IllegalAccessException e) {

				e.printStackTrace();
			}
		}
		
		
		if (nomeTracciatoOutput.equals("REP06") || nomeTracciatoOutput.equals("REP07") || nomeTracciatoOutput.equals("REP09") ) {
			Field field=null;
			String[] dataArrayName= {"workdate","datadocumento","dataaperturapratica","datamodificapratica"};
			for (int i = 0; i < dataArrayName.length; i++) {
				try {
					field=obj.getClass().getDeclaredField(dataArrayName[i]);
					field.setAccessible(true);
					String value=(String) field.get(obj);
					if (value==null || value.equals("")) {
						Field field2=obj.getClass().getDeclaredField("datatipoazione");
						field2.setAccessible(true);
						value=(String) field2.get(obj);
						field.set(obj, value);
					} 
				} catch (NoSuchFieldException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (SecurityException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalArgumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			
			
		}
		
	}

	public static boolean isNumeric(String str) {
		try {
			Integer.parseInt(str);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}
	
	public static boolean isNumericRegEx(String str) {
		boolean isNumeric=false;
		Pattern pattern = Pattern.compile("\\d+");
		    if (str == null) {
		        return false; 
		    }
		    isNumeric=pattern.matcher(str).matches();
		    return isNumeric;
	}

	private String concatenate(String[] element, String carattereSeparatore) {
		String result = "";
		int arrayMaxIndex = element.length - 1;
		for (int i = 0; i < element.length; i++) {
			if (element[i] != null & !element[i].equals("")) {
				result += element[i];
				if (carattereSeparatore != null & !carattereSeparatore.equals("") & i != arrayMaxIndex) {
					result += carattereSeparatore;
				}
			}
		}
		return result;
	}

	private Map<String, String> extractProperties(String path, String[] fields, boolean startWith) {
		Properties prop = new Properties();
		Map<String, String> fieldsResult = new LinkedHashMap<String, String>();
		try (InputStream xmlProperties = Files.newInputStream(Paths.get(path), StandardOpenOption.READ)) {
			prop.load(xmlProperties);
		} catch (Exception e) {
			log.info("File di Properties non trovato: " + path);
			e.printStackTrace();
		}

		// Se field è nullo vogliamo tutti i campi del file di properties
		if (fields == null) {
			for (Object key : prop.keySet()) {
				String str = key.toString();
				fieldsResult.put(str, prop.getProperty(str));
			}

		} else {
			if (startWith) {
				Set<Object> set = prop.keySet();
				for (int i = 0; i < fields.length; i++) {
					for (Object obj : set) {
						String str = obj.toString();

						if (str.startsWith(fields[i])) {
							String field = str.replace(fields[i], "");
							fieldsResult.put(field, prop.getProperty(str));
						}
					}

				}

			} else {

				for (int i = 0; i < fields.length; i++) {
					String valore = prop.getProperty(fields[i]);
					fieldsResult.put(fields[i], valore);
				}
			}
		}
		return fieldsResult;

	}

	private Properties extractProperties(String path) {
		Properties prop = new Properties();
		Map<String, String> fieldsResult = null;
		try (InputStream xmlProperties = Files.newInputStream(Paths.get(path), StandardOpenOption.READ)) {
			prop.load(xmlProperties);
		} catch (Exception e) {
			log.info("File di Properties non trovato");
		}

		return prop;

	}

	private String[] cercaRigaInFilediSupporto(String numeroPratica, File fileDiSupporto) {
		String[] result = new String[2];

		try {
			FileReader reader = new FileReader(fileDiSupporto);
			BufferedReader br = new BufferedReader(reader);
			// read the first line from the text file
			String line = br.readLine();
			int lineNumber = 0;
			int numPraticaIndex = -1;
			String[] valoriRigaArray = null;
			Boolean numPraticaExist = true;

			// loop until all lines are read
			while (line != null & numPraticaExist) {
				if (lineNumber == 0) {
					result[0] = line;
					String[] campi = line.split("#");
					for (int i = 0; i < campi.length; i++) {
						if (campi[i].equals("NUMERO_PRATICA")) {
							numPraticaIndex = i;
							break;
						}
					}
					if (numPraticaIndex == -1) {
						numPraticaExist = false;
					}
					lineNumber++;
					line = br.readLine();

				}
				// use string.split to load a string array with the values from
				// each line of
				// the file, using a comma as the delimiter
				valoriRigaArray = line.split("#");

				if (valoriRigaArray[numPraticaIndex].equals(numeroPratica)) {

					result[1] = line;
					break;
				}

				line = br.readLine();

			}
			br.close();

		} catch (IOException ioe) {
			ioe.printStackTrace();
		}

		return result;
	}
	


	void readUnzippedFolders(File directory) {

		Map<String, String> mapZipOutput = extractProperties(propertiesPath + "mapping_zip_output.properties", null,
				false);

		try {
			directoryInElab = "";
			String nomeDirectory = directory.getName();
			directoryInElab = directory.getPath();
			xmlOutputName = null;
			String nomeTracciatoOutput = null;

			if (!nomeDirectory.contains(INTESTAZIONE_ZIP)) {
				return;
			} else {
				nomeDirectory.replace(INTESTAZIONE_ZIP, "");
				boolean daElaborare = false;
				String value = null;
				int valueLen = 0;
				for (Map.Entry<String, String> entry : mapZipOutput.entrySet()) {

					// In questo if controllo se il nome della directory contiene uno dei nomi zip
					// contenuti nel file di properties
					// inoltre effettuo un ocntrollo sulla grandezza della stringa recuperata da
					// properties.
					// Motivazione ad esempio CON-> Rep09 e CONTAB->Rep03 se il nome file contine
					// solo con allora devo recuperare il REP09
					// se invece contiene la stringa contab devo recuperare il REP03; senza questo
					// controllo se in input abbiamo CONTAB, potrebbe fare match
					// con CON-> Rep09 anzichè CONTAB->Rep03
					if (nomeDirectory.contains(entry.getKey()) & entry.getKey().length() > valueLen) {
						value = entry.getValue();
						valueLen = value.length();
						daElaborare = true;
					}
				}

				xmlOutputName = nomeDirectory;
				nomeTracciatoOutput = value;
				if (!daElaborare) {
					return;
				}
			}

			File[] files = directory.listFiles(File::isFile);
			Map<String, File> csvFile = new HashMap<String, File>();
			for (int j = 0; j < files.length; j++) {
				log.info("Itero i file per ricercare il csv");
				String name = files[j].getName();
				String[] filename = name.split(Pattern.quote("."));
				if (filename.length > 0) {

					String extension = filename[filename.length - 1];
					if (extension.equals("csv")) {
						if (name.contains(INTESTAZIONECSVPRINCIPALE)) {
							log.info("Csv principale trovato");
							csvFile.put("PRINCIPALE", files[j]);
						} else if (name.contains(INTESTAZIONECSVSUPPORTO)) {
							log.info("Csv supporto trovato");
							csvFile.put("SUPPORTO", files[j]);
						} // END else if(name.contains(INTESTAZIONECSVSUPPORTO))
					} // END extension.equals("csv")
				} else {
					continue;
				} // END ELSE (filename.length > 0)
			}
			if (csvFile.size() != 0) {
				
				readCSV(nomeTracciatoOutput, csvFile);

			} else {
				log.info("Nessun cvs utile da leggere nella directory: " + directoryInElab);
			}

		} catch (Exception ioe) {
			ioe.printStackTrace();
		}

	}

	private static String formatDate(String stringDate, String oralegale) throws Exception {
		LocalDateTime localDateTime=null;
		String formattedDate = null;
		
		String[] values =stringDate.split(" ");
		if (values.length==1) {
			String[] gMa=values[0].split("/");
			localDateTime = 
			        LocalDateTime.of(Integer.parseInt(gMa[2]), Integer.parseInt(gMa[1]), Integer.parseInt(gMa[0]), 00, 00, 00, 0000);
			if ("YES".equals(oralegale)) {
			formattedDate=localDateTime.toString()+":00.000+01:00";
			} else {
				formattedDate=localDateTime.toString()+":00.000";
			}
		}
		
		if (values.length==2) {
			String[] gMa=values[0].split("/");
			String[] tempo=values[1].split(":");
			
		
			
			localDateTime = 
			        LocalDateTime.of(Integer.parseInt(gMa[2]), Integer.parseInt(gMa[1]), Integer.parseInt(gMa[0]), Integer.parseInt(tempo[0]), Integer.parseInt(tempo[1]), Integer.parseInt(tempo[2]), 0000);
			
			formattedDate=localDateTime.toString();
			if ("00".equals(tempo[2])) 
				formattedDate=formattedDate+":00";
		
			
			if ("YES".equals(oralegale)) {
				formattedDate=formattedDate+".000+01:00";
			} else {
				formattedDate=formattedDate+".000";
			}
		}

        return formattedDate;

    }

	
	void readUnzippedFoldersNew(File directory) {
		
		
		
		Map<String, String> connectionValues = extractProperties(propertiesPath + "dbConnection.properties", null,
				false);
		
		URL=connectionValues.get("URL");
		USR=connectionValues.get("USER");
		PWD=connectionValues.get("PASSWORD");
		try {
			log.info("Cerco di instaurare la connessione al db"  +"  "+ new Timestamp(System.currentTimeMillis()));
			myConn = DriverManager.getConnection(URL, USR,
					PWD);
			myConn.setAutoCommit(false);
			log.info("Connesso al db"  +"  "+ new Timestamp(System.currentTimeMillis()));
		} catch (Exception e) {
			log.error("Nessuna connessione al db----chiudo tutto"  +"  "+ new Timestamp(System.currentTimeMillis()));
			e.printStackTrace();
			System.exit(0);
		}

		Map<String, String> mapZipOutput = extractProperties(propertiesPath + "mapping_zip_output.properties", null,
				false);
		
		

		try {
			directoryInElab = "";
			nomeDirectory= directory.getName();
			directoryInElab = directory.getPath();
			xmlOutputName = null;
			nomeTracciatoOutput = null;
			

			if (!nomeDirectory.contains(INTESTAZIONE_ZIP)) {
				log.error("Folder che non devo elaborare: " + directoryInElab);
				return;
			} else {
				nomeDirectory.replace(INTESTAZIONE_ZIP, "");
				boolean daElaborare = false;
				String value = null;
				int valueLen = 0;
				for (Map.Entry<String, String> entry : mapZipOutput.entrySet()) {

					// In questo if controllo se il nome della directory contiene uno dei nomi zip
					// contenuti nel file di properties
					// inoltre effettuo un ocntrollo sulla grandezza della stringa recuperata da
					// properties.
					// Motivazione ad esempio CON-> Rep09 e CONTAB->Rep03 se il nome file contine
					// solo con allora devo recuperare il REP09
					// se invece contiene la stringa contab devo recuperare il REP03; senza questo
					// controllo se in input abbiamo CONTAB, potrebbe fare match
					// con CON-> Rep09 anzichè CONTAB->Rep03
					if (nomeDirectory.contains(entry.getKey()) & entry.getKey().length() > valueLen) {
						value = entry.getValue();
						valueLen = entry.getKey().length();
						daElaborare = true;
					}
				}

				xmlOutputName = nomeDirectory;
				nomeTracciatoOutput = value;
				if (!daElaborare) {
					return;
				}
			}

//			File[] files = directory.listFiles(File::isFile);
//			int pdfNum=files.length;
			errorLog=new ERRORLOG();
			errorLog.setTIPOTRACCIATO(nomeTracciatoOutput);
			errorLog.setNOMEZIP(nomeDirectory);
			Map<String, File> csvFile = new HashMap<String, File>();
			File csvPrincipale=null;
			File csvSupporto=null;
			try {
			csvPrincipale= new File(directory.getAbsolutePath()+osSeparatore+directory.getName()+".csv");
			if (csvPrincipale.exists()) {
				csvFile.put("PRINCIPALE", csvPrincipale);
//				pdfNum--;
			}
			else {
				String fileName= directory.getName().replace(INTESTAZIONECSVPRINCIPALE, INTESTAZIONECSVPRINCIPALE2);
				csvPrincipale= new File(directory.getAbsolutePath()+osSeparatore+fileName+".csv");
				if (csvPrincipale.exists()) {
					csvFile.put("PRINCIPALE", csvPrincipale);
//					pdfNum--;
				}
			}
			} catch (Exception e) {
				errorLog.setDESCRIZIONEERRORE("Problemi con il recupero del CSV Principale");
				log.error("Problemi con il recupero del CSV Principale");
			}
			try {
				String supportoName=directory.getName().replace(INTESTAZIONECSVPRINCIPALE,INTESTAZIONECSVSUPPORTO);
				csvSupporto= new File(directory.getAbsolutePath()+osSeparatore+supportoName+".csv");
				if (csvSupporto.exists()) {
					csvFile.put("SUPPORTO", csvSupporto);
//					pdfNum--;
				}
				} catch (Exception e) {
					errorLog.setDESCRIZIONEERRORE("Problemi con il recupero del CSV Principale");
					log.error("Problemi con il recupero del CSV di Supporto");
				}
			 
			if (csvFile.size() != 0 || errorLog.getDESCRIZIONEERRORE()!= null ) {
				
//				log.info("Presenti N° pdf: "+pdfNum);			
				
				log.info("Inizio elaborazione csv");
				readCSV(nomeTracciatoOutput, csvFile);
				ZIPLOG ziplog= new ZIPLOG();
				ziplog.setNOMEZIP(nomeDirectory);
				ziplog.setDATAPRIMAELABORAZIONE(new Timestamp(System.currentTimeMillis()).toString());
				ziplog.setTIPOTRACCIATO(nomeTracciatoOutput);
//				ziplog.setTOTALEPDF(String.valueOf (pdfNum));
				ziplog.setTOTALERIGHEELABORATE(String.valueOf (numeroRigheElab));
				ziplog.setTOTALERIGHECSV(String.valueOf (numeroRigheCsv));
				boolean insered = insertIntoDB(ziplog,null);
				if (insered) {
				myConn.commit();
				}
				
			} else {
				errorLog.setDATAELABORAZIONE( new Timestamp(System.currentTimeMillis()).toString());
				errorLog.setDESCRIZIONEERRORE("CSV non presente o con intestazione non conforme");
				log.error(errorLog.getDESCRIZIONEERRORE() + " : " + directoryInElab);
				boolean insered = insertIntoDB(errorLog,null);
				if (insered) {
				myConn.commit();
				}
			}

		} catch (Exception ioe) {
			try {
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				log.error("Errore in fase di insert ERRORLOG");
			}
			ioe.printStackTrace();
		} finally {
			try {
				if (myConn != null && !myConn.isClosed())
					myConn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}

	
	
	
	// Input zipName e mappa che come chiave a il nome del Csv file e come valore il
	// file stesso
	private void readCSV(String outputName, Map<String, File> csvFiles) {

		Map<String, String> ObjectOutputfield = extractProperties(
				propertiesPath + "outputObject" + osSeparatore + outputName + ".properties", null, false);
		numeroRigheElab=0;
		numeroRigheCsv=0;
		TracciatoOutput resultObj = null;
		FileReader reader = null;
		BufferedReader br = null;
		String line = null;
		String error = null;
		int lineNumber = 0;
		// leggi le righe del csv
		try {
			reader = new FileReader(csvFiles.get("PRINCIPALE"));
			br = new BufferedReader(reader);

			// read the first line from the text file
			line = br.readLine();
			
			String intestazioneCampi = null;
			String[] intestazioneCampiArray = null;
			String[] intestazioneCampiArraySupporto = null;

			String valoriRiga = null;
			String[] valoriRigaArray = null;
			String[] valoriRigaArraySupporto = null;

			// loop until all lines are read
			while (line != null) {
				try {
					if (!line.equals("") && line.split("#").length > 1) {
						

						if (lineNumber == 0) {
							intestazioneCampi = line;
							intestazioneCampiArray = line.split("#");
							lineNumber++;

							// questa operazione di aggiornamento è stata spostata nel finally
							// line = br.readLine();
							continue;
						}
						// use string.split to load a string array with the values from
						// each line of
						// the file, using a comma as the delimiter
						lineNumber++;
						numeroRigheCsv++;
						valoriRiga = line;
						valoriRigaArray = line.split("#");
						errorLog.setDESCRIZIONEERRORE(null); 

						String numeroPratica = null;
						for (int i = 0; i < intestazioneCampiArray.length; i++) {
							if (intestazioneCampiArray[i].equals("NUMERO_PRATICA")) {
								numeroPratica = valoriRigaArray[i];
								break;
							}
						}
						if (csvFiles.get("SUPPORTO") != null) {
							String[] resultMetod = cercaRigaInFilediSupporto(numeroPratica, csvFiles.get("SUPPORTO"));

							intestazioneCampiArraySupporto = resultMetod[0].split("#");
							if (resultMetod[1] == null) {
								error = "Nessuna occorrenza nel file di supporto della pratica n°: " + numeroPratica;
								throw new Exception();
							}
							valoriRigaArraySupporto = resultMetod[1].split("#");

						}
						Class<?> TracciatoOutput;
						try {
							TracciatoOutput = Class.forName("generated." + outputName);
						} catch (ClassNotFoundException e) {
							error = "Impossibile generare la classe di output: " + outputName;
							throw new Exception("Impossibile generare la classe di output " + outputName);
						}
						Constructor[] constructors = TracciatoOutput.getConstructors();
						Constructor emptyConstructor = null;
						if (constructors[0].getParameterCount() == 0) {
							emptyConstructor = constructors[0];
						} else if (constructors[1].getParameterCount() == 0) {
							emptyConstructor = constructors[1];
						} else {
							error = "Impossibile trovare il costruttore vuoto della classe " + outputName;
							throw new Exception("Impossibile trovare il costruttore vuoto della classe " + outputName);

						}

						try {
							resultObj = (TracciatoOutput) emptyConstructor.newInstance();
						} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
								| InvocationTargetException e) {
							// TODO Auto-generated catch block
							error = "Impossibile istanziare l'oggetto " + outputName;
							throw new Exception("Impossibile istanziare l'oggetto " + outputName);
						}
						
						try {
						creaObjectFromCSVLine(intestazioneCampiArray, valoriRigaArray, intestazioneCampiArraySupporto,
								valoriRigaArraySupporto, resultObj, ObjectOutputfield);
						}catch (Exception e) {
							error = e.getMessage();
							throw new Exception(e.getMessage());
						}
						
						String finaloutputPath = xmlOutputPath;
						if (!finaloutputPath.substring(xmlOutputPath.length() - 1).equals(osSeparatore)) {
							finaloutputPath += osSeparatore;
							}
						
						String name = csvFiles.get("PRINCIPALE").getName();
						String nomeZIP=(nomeDirectory.replace(INTESTAZIONECSVPRINCIPALE, "")).replace(".csv", "");
						finaloutputPath+=nomeZIP+osSeparatore; 
						
						finaloutputPath += dataDoc.replace("/", "") + osSeparatore;
						String[] keyField = {"coddocumento","robjectid"};
						for (int i = 0; i < keyField.length; i++) {
							Field field;
							try {
								field = resultObj.getClass().getDeclaredField(keyField[i]);
								field.setAccessible(true);
							} catch (NoSuchFieldException | SecurityException e) {
								// TODO Auto-generated catch block
								
								error = "Problemi con il campo " + keyField[i] + " per la classe " + outputName;
								errorLog.setDESCRIZIONEERRORE(error);
								throw new Exception(
										"Problemi con il campo " + keyField[i] + " per la classe " + outputName);
							}

							if (!finaloutputPath.substring(finaloutputPath.length() - 1).equals(osSeparatore)) {
								finaloutputPath += osSeparatore;
							}
							try {
								String toAdd = field.get(resultObj).toString().replace("/", "");
								finaloutputPath += toAdd+osSeparatore;
							} catch (IllegalArgumentException | IllegalAccessException e) {
								error = "Impossibile recuperare il valore del campo " + keyField[i] + " per l'oggetto "
										+ outputName;
								throw new Exception("Impossibile recuperare il valore del campo " + keyField[i]
										+ " per l'oggetto " + outputName);
							}

						}

						
						Field field;
						try {
							field = resultObj.getClass().getDeclaredField("filename");
							field.setAccessible(true);
							pdfname = (String) field.get(resultObj);
						} catch (NoSuchFieldException | SecurityException | IllegalArgumentException
								| IllegalAccessException e) {
							// TODO Auto-generated catch block
							error = "Impossibile recuperare il valore del campo filename" + " per l'oggetto "
									+ outputName;
							errorLog.setDESCRIZIONEERRORE(error);
							throw new Exception("Impossibile recuperare il valore del campo filename"
									+ " per l'oggetto " + outputName);
						}

						if (pdfname == null && pdfname.equals("")) {
							error = "riga senza nome pdf ";
							errorLog.setDESCRIZIONEERRORE(error);
							throw new Exception("pdf indicato non esistente ");
						}

						if (pdfname.split("\\.").length > 0) {
							if ((!estensione.equals("") || estensione == null)
									&& !pdfname.split("\\.")[pdfname.split("\\.").length - 1].equals(estensione)) {
								pdfname += "." + estensione;
							}
						} else {
							pdfname += "." + estensione;
						}
						
						boolean insered = insertIntoDB(resultObj, finaloutputPath  + pdfname);
						
						Path pdfFrom = Paths.get(directoryInElab + osSeparatore + pdfname);
						Path pdfTo = Paths.get(finaloutputPath + pdfname);
						if (Files.notExists(pdfFrom)) {
							myConn.rollback();
							error = "pdf non presente nella cartella di input --> " + pdfname;
							if (errorLog.getDESCRIZIONEERRORE()==null) {
							errorLog.setDESCRIZIONEERRORE(error);
							throw new Exception(error);
							}
						}

						

						//if (true) {
						if (insered) {

							try {
								File file = new File(finaloutputPath);
								// Creating the directory
								file.mkdirs();

								Files.move(pdfFrom, pdfTo);
								String[] pdfnameArr=pdfname.split("\\.");
								String formato="."+pdfnameArr[pdfnameArr.length-1];
								String xmlName=pdfname.replace(formato, "");
								serializeToXML(resultObj, finaloutputPath, xmlName);
								numeroRigheElab++;
							//RaccordoAnagrafe raccanag=new RaccordoAnagrafe(propertiesPath);
							//raccanag.elabora(line);
							} catch (Exception e) {
								myConn.rollback();
								if (errorLog.getDESCRIZIONEERRORE()==null) {
								error = "Spostamento pdf o creazione XML fallito. Potrebbe già esistere il file: " + finaloutputPath
										 + pdfname;
								errorLog.setDESCRIZIONEERRORE(error);
								throw new Exception(error);
								}

							}
							myConn.commit();
						} else {
							error = "Errore in fase di inserimento in db";
							throw new Exception();
						}

						log.info("riga elaborata correttamente"  +"  "+ new Timestamp(System.currentTimeMillis()));
					} else {
						log.error("riga vuota o non valida");
					}

				} catch (Exception e) {
					log.error("errore in fase di elaborazione della riga ", e);
					errorLog.setNUMERORIGACSV(String.valueOf(lineNumber));
					errorLog.setDATAELABORAZIONE( new Timestamp(System.currentTimeMillis()).toString());
					errorLog.setDESCRIZIONEERRORE( e.getMessage());
					try {
						boolean insered = insertIntoDB(errorLog,null);
						if (insered) {
						myConn.commit();
						}
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						log.error("errore in fase di scrittura in ErrorLog", e);
					}
					
				} finally {
					try {
						line = br.readLine();
					} catch (IOException e) {
						log.error("errore in fase di passaggio a riga successiva", e);
						throw new IOException();
					}
				}

			}

		} catch (IOException ioe) {
			log.error("errore in fase di lettura csv");
			errorLog.setDATAELABORAZIONE( new Timestamp(System.currentTimeMillis()).toString());
			errorLog.setDESCRIZIONEERRORE("errore in fase di lettura csv  ");
			try {
				boolean insered = insertIntoDB(errorLog,null);
				if (insered) {
				myConn.commit();
				}
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				log.error("errore in fase di scrittura in ErrorLog", e1);
			}
		
		}

	}

	public void serializeToXML(TracciatoOutput obj, String toPath, String filename) throws IOException {

		XmlMapper xmlMapper = new XmlMapper();

		// serialize our Object into XML string
		String xmlString = xmlMapper.writeValueAsString(obj);


		// write to the console
		// log.info(xmlString);

		// write XML string to file
		File xmlOutput = new File(toPath + osSeparatore + filename + ".xml");
		xmlOutput.createNewFile();
		FileWriter fileWriter = new FileWriter(xmlOutput);
		fileWriter.write(xmlString);
		fileWriter.close();
	}

	public boolean insertIntoDB(Object object, String pdfpath) throws Exception {
		boolean insered = true;
		Class c = object.getClass();
		String className = c.getName().replace("generated.", "");
		String[] field = { className };

		Map<String, String> ObjectOutputfield = extractProperties(
				propertiesPath + "DbObjectMapping" + osSeparatore + "RepInsert.properties", field, false);

		String allCampi = ObjectOutputfield.get(field[0]);
		if (allCampi == null) {
			log.error(className + " non mappato nel file RepInsert.properties");
			throw new Exception();
		}
		String[] arrayCampi = allCampi.split("-");
		String query="";
		try {
			query = buildInsert(object, className, arrayCampi, pdfpath);
			//log.info(query);
			stmt = myConn.prepareStatement(query);
			stmt.executeQuery(query);
			stmt.close();

		} catch (Exception e) {
			log.error("Primo tentativo di insert fallito");
			myConn.close();
			myConn=null;
			boolean riconnected=false;
			if (myConn==null) {
				riconnected=riconnessioneDb();
				if (!riconnected) {
					log.info("Ho provato la riconnessione ma non riesco a ristabilirla");
					log.info("Killo il processo");
					System.exit(0);
				}
				
				stmt = myConn.prepareStatement(query);
				try {
				stmt.executeQuery(query);
				stmt.close();
				}catch (Exception e1) {
					log.error("Secondo tentativo di insert fallito --> "+ e.getMessage() );
					e.printStackTrace();
					if (errorLog.getDESCRIZIONEERRORE()==null) {
						errorLog.setDESCRIZIONEERRORE("Errore in fase di inserimento in db --> "+e.getMessage() );
						throw new Exception("Errore in fase di inserimento in db --> "+e.getMessage());
						}
					
					insered = false;
				}
			}
		
			
			try {
				if (stmt != null && !stmt.isClosed())
					stmt.close();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				//e1.printStackTrace();
				log.error(e1.toString());
			}
		}

		return insered;
	}
	
	public boolean riconnessioneDb() {
		boolean riconnesso= false;
		int tentativo=0;
		while(tentativo<100) {
			tentativo++;
			try {
				myConn = DriverManager.getConnection(URL,USR,
						PWD);
				myConn.setAutoCommit(false);
			} catch (SQLException e) {
				log.info("Errore in fase di riconnessione al db");
				e.printStackTrace();
			}
			if (myConn!=null) {
				return true;
			}
		}
		return riconnesso;
	}
	
	
	public String buildInsert(Object object, String tabella, String[] arrayCampi, String pdfpath)
			throws Exception {
		String query = "INSERT INTO " + tabella + " VALUES(";

		for (int i = 0; i < arrayCampi.length; i++) {
			String nomeCampoOggetto = arrayCampi[i].split("\\*")[0];
			
			if (nomeCampoOggetto.equals("zipinput")) {
				query += "'" + nomeDirectory + "',";
				continue;}
			
			
			if (nomeCampoOggetto.equals("codappl")) {
				String codappl="";
				switch (tabella) { 
				case "REP06": codappl="6"; break; 
				case "REP07": codappl="7"; break;  
				case "REP09": codappl="9"; break;
				}
				
				if (!"".equals(codappl)){
					query += "'" + codappl + "',";
				continue;}
				
				
			}
			if (nomeCampoOggetto.equals("filepath")) {
				query += "'" + pdfpath + "',";
				continue;				
			} 
			else if (nomeCampoOggetto.equals("id")) {
				query += "'" + UUID.randomUUID() + "',";
				continue;
			} else {
				if (nomeCampoOggetto.equals("sip_created") || nomeCampoOggetto.equals("sip_transfered")
						|| nomeCampoOggetto.equals("sip_archived")) {
					query += "'N',";
					continue;
				}
			}
			String tipo = arrayCampi[i].split("\\*")[1];
			Field field;
			String fieldValue = null;
			try {
				field = object.getClass().getDeclaredField(nomeCampoOggetto);
				field.setAccessible(true);
				fieldValue = (String) field.get(object);
			} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
				// TODO Auto-generated catch block
				log.info("Impossibile recuperare il valore del campo " + nomeCampoOggetto);
				throw new Exception(
						"Impossibile recuperare il valore del campo filename" + " per l'oggetto " + nomeCampoOggetto);
			}
			switch (tipo) {
			case "stringa":
				if (fieldValue != null) {
					query += "'" + fieldValue + "',";
				} else {
					query += "null,";
				}
				break;

			case "numero":
				int n = -1;
				if (fieldValue!=null) {
				try {
					n = Integer.parseInt(fieldValue);
				} catch (NumberFormatException e) {
					log.info("Numero non valido");
				}
				}
				if (n != -1) {
					query += n + ",";
				}	 else {
					query += "null,";
				}
				
				break;

			case "data":
				if (fieldValue != null) {
					String data=fieldValue.split("T")[0];
					data=data.replace("-", "/");
					query += "to_date('" + data + "','YYYY/MM/DD'),";
				} else {
					query += "null,";
				}
				break;

			default:
				break;
			}

		}
		StringBuilder builder = new StringBuilder(query);
		builder.replace(query.length() - 1, query.length(), ")");
		query = builder.toString();
		return query;
	}

}
