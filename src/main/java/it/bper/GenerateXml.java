package it.bper;


import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;



class GenerateXml{


	
	private static Logger log=null;

	private static final String osSeparatore=File.separator;
	
	int numberParamenters = 0;

	
	static String propertiesPath;
	private static String inputDir;
	private static String xmlOutputPath;
	
	
	public static void main(String[] args) {
		
		
	    
		//logger.info("No Multithreading");
		if (args.length==3) {
			inputDir = args[0];
			xmlOutputPath = args[1];
			propertiesPath = args[2] + osSeparatore;
		} else {
			log.info("Parametri di input non corretti");
			log.info("Primo parametro: path input directory");
			log.info("Secondo parametro: xmlOutputPath");
			log.info("Terzo parametro: Properties file path");
			return;
		}
		 SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy_HH-mm-ss");  
		    Date date = new Date();  
		    
		
		System.setProperty("my.log", formatter.format(date)+".log");
		log = Logger.getLogger("XMLCreator");
		log.info("Inizio Elaborazione: " +  new Timestamp(System.currentTimeMillis()));
		
		
			//readFolders(inputDir);
			File[] directories = new File(inputDir).listFiles(File::isDirectory);
			log.info("Leggo le directories in nella folder di input");
			
			
			for (int i = 0; i < directories.length; i++) {
				boolean existPdf=false;
				log.info("Leggo la directory "+directories[i].getName() +"  "+ new Timestamp(System.currentTimeMillis()));
				Utils util=new Utils(xmlOutputPath,propertiesPath);
				util.readUnzippedFoldersNew(directories[i]);
				
				
				//parte per spostare le directory elaborate, attualmente non funziona per problemi di permessi su file csv da spostare
//				File elaboratiFolder=new File (inputDir+osSeparatore+"elaborati");
//				elaboratiFolder.mkdir();
//				File[] files = directories[i].listFiles(File::isFile);
//				for (int j = 0; j < files.length; j++) {
//					log.info("Itero i file per controllare se esiste ancora un pdf ");
//					String name = files[j].getName();
//					String[] filename = name.split(Pattern.quote("."));
//					if (filename.length > 0) {
//
//						String extension = filename[filename.length - 1];
//						if (extension.equals("pdf")) {
//							existPdf=true;
//							break;
//							}
//						}
//					
//				}
//				if (!existPdf) {
//					try {
//						File from=new File (directories[i].getAbsolutePath());
//						from.setWritable(true);
//						File to=new File (elaboratiFolder.getAbsolutePath()+osSeparatore+directories[i].getName());
//						
//						FileUtils.moveDirectory(from,to );
//					} catch (IOException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					}
//			} else {
//				directories[i].renameTo(new File(directories[i].getAbsolutePath()+osSeparatore+"notCompleted_"+directories[i].getName()));
//			}
			
			
			}
			//currentTime = LocalDateTime.now();
			log.info("Fine Elaborazione: " +"  "+ new Timestamp(System.currentTimeMillis()));
	}
}
