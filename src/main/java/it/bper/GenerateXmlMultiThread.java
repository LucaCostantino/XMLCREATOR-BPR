package it.bper;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

import java.util.HashMap;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

import java.util.regex.Pattern;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.fasterxml.jackson.core.JsonProcessingException;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;



import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;


import generated.TracciatoOutput;



class GenerateXmlMultiThread{

	private static final Logger logger = LogManager.getLogger("SipGeneratorApplication");

	private static final String INTESTAZIONE_ZIP="BPR.DOC.DOC";
	private static final String	INTESTAZIONECSVPRINCIPALE="BPR.DOC.DOC";
	private static final String INTESTAZIONECSVSUPPORTO="BPR.DOC.PRA";
	private static final String osSeparatore=File.separator;
	
	int numberParamenters = 0;
	static String NomeInput;
	static String TracciatoType;
	static int sipCount = 0;
	static String propertiesPath;
	static String TracciatoOutputType;
	private static String inputDir;
	private static String xmlOutputPath;
	private static String xmlOutputName;
	private static String directoryInElab;

//	
//	public static void main(String[] args) {
//		BasicConfigurator.configure();
//		
//		if (args.length==3) {
//			inputDir = args[0];
//			xmlOutputPath = args[1];
//		} else {
//			System.out.println("Parametri di input non corretti");
//			System.out.println("Primo parametro: path input directory");
//			System.out.println("Secondo parametro: xmlOutputPath");
//			System.out.println("Terzo parametro: Properties file path");
//			return;
//		}
//		
//			propertiesPath = args[2] ;
//			File[] directories = new File(inputDir).listFiles(File::isDirectory);
//			for (int i = 0; i < directories.length; i++) {
//				String threadName= "Thread_"+i;
//				SingleThread T1 = new SingleThread(threadName,xmlOutputPath,propertiesPath,directories[i]);
//				T1.start();
//			}
//	}


	
}
